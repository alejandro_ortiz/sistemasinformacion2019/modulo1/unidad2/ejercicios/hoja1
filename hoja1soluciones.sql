﻿USE ciclistas;

-- 1. Listar las edades de los ciclistas (sin repetidos)

  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c;

-- 2. Listar las edades de los ciclistas de Artiach

  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Artiach';

-- 3. Listar las edades de los ciclistas de Artiach o de Amore Vita

-- Solución con el operador OR

  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Artiach' 
    OR 
    c.nomequipo='Amore Vita';

-- Solución con el operador extendido IN

  SELECT 
    DISTINCT c.edad 
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo IN('Artiach','Amore Vita');

-- Solución con el operador UNION

  SELECT 
    DISTINCT c.edad
  FROM 
    ciclista c 
  WHERE 
    c.nomequipo='Artiach'
  UNION
  SELECT 
    DISTINCT c.edad
  FROM 
    ciclista c   
  WHERE 
    c.nomequipo='Amore Vita';

-- 4. Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30

-- Solución con el operador OR

  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.edad<25 
    OR 
    c.edad>30;

 -- Solución con el operador UNION

  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.edad<25
  UNION
  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.edad>30;

-- Solución con el operador NOT BETWEEN

  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.edad NOT BETWEEN 25 AND 30;

-- Solución comn el operador NOT IN

  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.edad NOT IN(25,26,27,28,29,30);

-- 5. Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y además que solo sean de Banesto

  SELECT 
    c.dorsal 
  FROM 
    ciclista c 
  WHERE 
    c.edad BETWEEN 28 AND 32 
    AND 
    c.nomequipo='Banesto';

-- 6. Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8

  SELECT 
    DISTINCT c.nombre 
  FROM 
    ciclista c 
  WHERE 
    CHAR_LENGTH(c.nombre)>8;

-- 7. Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas
  
  SELECT 
    c.nombre, 
    c.dorsal, 
    UPPER(c.nombre)Nombre_Mayusculas 
  FROM 
    ciclista c;

-- 8. Listar todos los ciclistas que han llevado el maillot MGE (amarillo) en alguna etapa.

  SELECT 
    DISTINCT l.dorsal 
  FROM 
    lleva l 
  WHERE 
    l.código='MGE';

-- 9. Listar el nombre de los puertos cuya altura sea mayor que 1500.

  SELECT 
    DISTINCT p.nompuerto 
  FROM 
    puerto p 
  WHERE 
    p.altura>1500;

-- 10. Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000

-- Solucion con operador OR
  
  SELECT 
    DISTINCT p.dorsal 
  FROM 
    puerto p 
  WHERE 
    p.pendiente>8 
    OR 
    p.altura BETWEEN 1800 AND 3000;

-- Solucion con operador UNION 

  SELECT 
    DISTINCT p.dorsal 
  FROM 
    puerto p 
  WHERE 
    p.pendiente>8 
  UNION
  SELECT 
    DISTINCT p.dorsal 
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1800 AND 3000;
   
-- 11. Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000

  SELECT 
    DISTINCT p.dorsal 
  FROM 
    puerto p 
  WHERE 
    p.pendiente>8 
    AND
    p.altura BETWEEN 1800 AND 3000;